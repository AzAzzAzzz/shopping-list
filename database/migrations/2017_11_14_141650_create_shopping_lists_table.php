<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('count')->unsigned()->comment('Количество продукта')->default(0);
            $table->text('comment', 500)->nullable();
            $table->integer('user_id')->unsigned()->comment('Идентификатор пользователя');
            $table->integer('category_id')->unsigned()->comment('Идентификатор категории продуктов');
            $table->integer('unit_id')->unsigned()->comment('Идентификатор еденицы измерения');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('unit_id')->references('id')->on('units');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_lists');
    }
}
