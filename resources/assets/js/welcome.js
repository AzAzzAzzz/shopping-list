jQuery(function () {
    var addListForm = jQuery('#add-lists-form'),
        productListTable = jQuery('#product-list-table'),
        listRowTemplate = jQuery('#list-row').html();

    var table = productListTable.DataTable({
        'language' : {
            'url': '//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json'
        }
    });

    // Adding new unit
    addListForm.validate({
        debug: true,
        rules: {
            name: {
                required: true,
                minlength: 4,
                maxlength: 255
            },
            unit: {
                number: true
            },
            count: {
                maxlength: 6,
                number: true
            },
            comment: {
                maxlength: 350
            }
        },
        submitHandler: function (form) {
            var addBtn = addListForm.find('.add-btn');
            addBtn.attr('disabled', true);

            jQuery.ajax({
                url: jQuery(form).attr('action'),
                type: jQuery(form).attr('method'),
                headers: { 'X-CSRF-TOKEN': _token },
                data: jQuery(form).serialize()
            }).done(function (response) {
                if (response.error === true) {
                    alertify.logPosition("top right").error(response.message);
                    return false;
                }

                addListForm.trigger('reset');

                Mustache.parse(listRowTemplate);

                var row = jQuery(Mustache.render(listRowTemplate, response.data));

                row
                    .find('.table_unit option[value="' + response.data.unit_id + '"]')
                    .attr('selected', true);
                row
                    .find('.table_category option[value="' + response.data.category_id + '"]')
                    .attr('selected', true);

                // Add row
                table.rows.add(row).draw();

                addBtn.attr('disabled', false);
                alertify.logPosition('top right').success(response.message);
            });

            return true;
        }
    });


    // Destroy column
    jQuery('body').on('click', '.btn-remove', function () {
        var element = jQuery(this).parents('tr'),
            template = jQuery('#destroy-row').html();

        Mustache.parse(template);

        alertify.confirm(Mustache.render(template, {'name': element.find('.row-name').val()}), function () {
            // user clicked "ok"

            var id = element.find('.row-id').val(),
                data = element.find('input, select, textarea').serialize();


            jQuery.ajax({
                url: '/shopping_list/' + id,
                type: 'DELETE',
                headers: { 'X-CSRF-TOKEN': _token },
                data: data
            }).done(function (response) {
                if (response.error === true) {
                    alertify.logPosition("top right").error(response.message);
                    return false;
                }

                element.remove();
                alertify.logPosition("top right").success(response.message);
            });
        }, function() {
            // user clicked "cancel"
            return true;
        });
    });

    // Update column data
    jQuery('body').on('click', '.btn-save', function () {
        var element = jQuery(this).parents('tr'),
            id = element.find('.row-id').val(),
            data = element.find('input, select, textarea').serialize();

        var name = element.find('.row-name').val(),
            count = element.find('.row-count').val(),
            comment = element.find('.row-comment').val();

        // Validate input fields
        var reg = /^\d+$/,
            error = false;

        if (name.length < 4) {
            element.find('.row-name').addClass('error');
            error = true;
        }
        if ( ! reg.test(count)) {
            element.find('.row-count').addClass('error');
            error = true;
        }
        if (comment.length > 350) {
            element.find('.row-comment').addClass('error');
            error = true;
        }

        if (error === true) {
            return false;
        }

        jQuery.ajax({
            url: '/shopping_list/' + id,
            type: 'PUT',
            headers: { 'X-CSRF-TOKEN': _token },
            data: data
        }).done(function (response) {
            if (response.error === true) {
                alertify.logPosition("top right").error(response.message);
                return false;
            }

            alertify.logPosition("top right").success(response.message);
        });
    });

    jQuery('#product-list-table input, textarea').on('keyup', function() {
       if (jQuery(this).hasClass('error')) {
           jQuery(this).removeClass('error');
       }
    });
});