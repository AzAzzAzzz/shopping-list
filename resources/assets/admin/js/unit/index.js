jQuery(function () {
    var table = jQuery('#unit-table'),
        dateFormat = 'DD MMMM YYYY в hh:mm';

    table.DataTable({
        "ajax": '/admin/unit/getTableData',
        'language' : {
            'url': '//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json'
        },
        "columns": [
            { "data":
                "name",
                "render": function (data, type, row) {
                    return row.name
                }
            },
            { "data":
                "created_at" ,
                "render": function (data, type, row) {
                    return row.created_at ? moment(row.created_at).format(dateFormat) : '';
                }
            },
            { "data":
                "updated_at" ,
                "render": function (data, type, row) {
                    return row.updated_at ? moment(row.updated_at).format(dateFormat) : '';
                }
            },
            { "data":
                "action",
                'render':  function (data, type, row) {
                    var template = jQuery('#actionButtonsTemplate').html();
                    Mustache.parse(template);
                    return Mustache.render(template, row);
                }
            }
        ]
    });

    /**
     * Удаление категории
     */
    jQuery('body').on('click', '.btn-unit-destroy', function () {
        var element = jQuery(this),
            id = element.data('id'),
            name = element.data('unit-name'),

            template = jQuery('#destroy-unit').html();

        Mustache.parse(template);

        alertify.confirm(Mustache.render(template, {'name': name}), function () {
            // user clicked "ok"
            NProgress.start();

            jQuery.ajax({
                url: '/admin/unit/destroy/' + id,
                type: 'DELETE',
                headers: { 'X-CSRF-TOKEN': _token },
                data: {
                    id: id
                }
            }).done(function (response) {
                NProgress.done();

                if (response.error === true ) {
                    alertify.logPosition("top right").error(response.message);
                    return false;
                }

                // Обновление таблицы
                table.DataTable().ajax.reload();

                alertify.logPosition("top right").success(response.message);
            });
        }, function() {
            // user clicked "cancel"
            return true;
        });

    });
});