jQuery(function () {

    // Adding new unit
    jQuery('#unit-form').validate({
        debug: true,
        rules: {
            name: {
                required: true,
                minlength: 2,
                maxlength: 30
            }
        },
        submitHandler: function (form) {
            NProgress.start();

            jQuery.ajax({
                url: jQuery(form).attr('action'),
                type: jQuery(form).attr('method'),
                headers: { 'X-CSRF-TOKEN': _token },
                data: jQuery(form).serialize()
            }).done(function (response) {
                NProgress.done();

                if (response.error === true) {
                    alertify.logPosition("top right").error(response.message);
                    return false;
                }

                jQuery('#unit-form').trigger('reset');
                alertify.logPosition('top right').success(response.message);
            });

            return true;
        }
    });
});
