jQuery(function () {
    /**
     *  Добавление новой категории
     */
    jQuery('#category-form').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            }
        },
        submitHandler: function (form) {
            NProgress.start();

            jQuery.ajax({
                url: jQuery(form).attr('action'),
                type: jQuery(form).attr('method'),
                headers: { 'X-CSRF-TOKEN': _token },
                data: jQuery(form).serialize()
            }).done(function (response) {
                NProgress.done();

                if (response.error === true) {
                    alertify.logPosition("top right").error(response.message);
                    return false;
                }

                jQuery('#category-form').trigger('reset');
                alertify.logPosition('top right').success(response.message);
            });

            return true;
        }
    });
});
