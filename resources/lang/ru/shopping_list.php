<?php

return [
    'add_list_success'     => 'Новая запись успешно добавлены.',
    'add_list_error'       => 'Не удалось добавить новую запись.',
    'update_list_error'    => 'Не удалось обновить даынне.',
    'update_list_success'  => 'Данные записи успешно обновлены.',
    'destroy_list_error'   => 'Не удалось удалить данные записи.',
    'destroy_list_success' => 'Данные записи успешно удалены.',
];