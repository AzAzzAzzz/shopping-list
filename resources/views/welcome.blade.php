@extends('layouts.welcome')

@section('content')

    @if (Auth::check())
        @include('sections.welcome.table')
    @else
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <strong>Данные для авторизации:</strong>
                    <p>e-mail: admin.laravel@labs64.com / пароль: admin</p>
                </div>
            </div>
        </div>
    @endif

    <script id="destroy-row" type="text/template">
        <p>{{ __('views.welcome.warning_1') }} - @{{ name }}?</p>
    </script>

    <script id="list-row" type="text/template">
        <tr>
            <td data-order="@{{ name }}">
                <input type="hidden" class="row-id" name="id" value="@{{ id }}">
                <input type="text" class="form-control row-name" name="table_name" value="@{{ name }}">
            </td>
            <td data-order="@{{ count }}">
                <input type="text" class="form-control table_count row-count" name="table_count" value="@{{ count }}">
            </td>
            <td data-order="@{{ count }}">
                <select class="table_unit form-control" name="table_unit" class="form-control">
                    @foreach($units as $unit)
                        <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select class="table_category form-control" name="table_category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <textarea name="table_comment"  class="form-control row-comment" rows="1">@{{ comment }}</textarea>
            </td>
            <td>
                @{{ created_at }}
            </td>
            <td>
                <div class="table-actions">
                    <button class="btn btn-danger btn-remove">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-success btn-save">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    </button>
                </div>
            </td>
        </tr>
    </script>
@endsection

@section('scripts')
    @parent

    {{ Html::script(mix('assets/js/welcome.js')) }}
@endsection
