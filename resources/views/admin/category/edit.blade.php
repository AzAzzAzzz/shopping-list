@extends('admin.layouts.admin')

@section('title', __('views.admin.category.edit.title'))

@section('content')

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ __('views.admin.category.edit.header_1') }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form id="edit-form" action="{{ route('admin.categories.update', $category->id ) }}" method="PUT" class="form-horizontal">
                        <input type="hidden" name="id" value="{{ $category->id }}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                {{ __('views.admin.category.edit.label_1') }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text"
                                       name="name"
                                       id="name"
                                       class="form-control"
                                       value="{{ $category->name }}"
                                       placeholder="{{ __('views.admin.category.edit.placeholder_1') }}">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary" type="reset">{{ __('views.admin.category.edit.btn_title_1') }}</button>
                                <button type="submit" class="btn btn-success">{{ __('views.admin.category.edit.btn_title_2') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/category/edit.js')) }}
@endsection
