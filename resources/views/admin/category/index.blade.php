@extends('admin.layouts.admin')

@section('title', __('views.admin.category.index.title'))

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ __('views.admin.category.index.header_1') }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li>
                            <a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="categories-table"
                           class="table table-striped table-bordered dt-responsive nowrap"
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>{{ __('views.admin.category.index.table_header_1') }}</th>
                                <th>{{ __('views.admin.category.index.table_header_2') }}</th>
                                <th>{{ __('views.admin.category.index.table_header_3') }}</th>
                                <th>{{ __('views.admin.category.index.table_header_4') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>{{ __('views.admin.category.index.table_header_1') }}</th>
                                <th>{{ __('views.admin.category.index.table_header_2') }}</th>
                                <th>{{ __('views.admin.category.index.table_header_3') }}</th>
                                <th>{{ __('views.admin.category.index.table_header_4') }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Шаблон предупреждения об удалении категории --}}
    <script id="destroy-category" type="text/template">
        <p>{{ __('views.admin.category.index.warning_1') }} "@{{ name }}"?</p>
    </script>


    {{-- Шаблон кнопок в колонках таблицы --}}
    <script id="actionButtonsTemplate" type="text/template">
        <a class="btn btn-xs btn-info"
           target="_blank"
           href="/admin/categories/@{{ id }}/edit"
           data-toggle="tooltip"
           data-placement="top"
           title="{{ __('views.admin.category.index.btn_title_1') }}  | {{ __('views.admin.category.index.btn_title_2') }}">
            <i class="fa fa-pencil"></i>
        </a>

        <button class="btn btn-xs btn-danger btn-category-destroy"
                data-toggle="tooltip"
                data-placement="top"
                title="{{ __('views.admin.category.index.btn_title_3') }}"
                data-id="@{{ id }}"
                data-category-name="@{{ name }}">
            <i class="fa fa-trash"></i>
        </button>
    </script>

@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/category/index.js')) }}
@endsection
