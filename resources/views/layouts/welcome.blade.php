<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Meta title & meta -->
        @meta

        <!-- Styles -->
        {{ Html::style(mix('assets/app.css')) }}
        @yield('styles')

        <!-- Laravel variables for js -->
        @tojs

    </head>
    <body>
        <!-- Wrapper -->
        <div id="app" class="conatainer">
            <div class="row">

                @include('sections.header')

                <!-- Main -->
                <div id="main">
                    @yield('content')
                </div>
                <!--/ Main -->

                @include('sections.footer')
            </div>
        </div>
        <!-- /Wrapper -->
        {{ Html::script(mix('assets/js/plugins.js')) }}
        {{ Html::script(mix('assets/app.js')) }}
        @yield('scripts')
    </body>
</html>
