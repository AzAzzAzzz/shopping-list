<!-- Header -->
<div id="header" class="alt">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="text-center">{{ env('APP_NAME') }}</h1>
                @if (Auth::check())
                    <a href="{{ route('logout') }}">{{ __('views.backend.section.header.menu_0') }}</a>
                @endif
            </div>
        </div>
    </div>
    <!-- TODO: destroy for production -->
    @if ( ! Auth::check())
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p>
                        Для просмотра или добавления списка покупок <br> пожалуйста,
                        <a href="{{ route('login') }}">Войдите</a> или <a href="{{ route('register') }}">Зарегиструрейтесь</a>.
                    </p>
                </div>
            </div>
        </div>
    @endif
</div>
<!-- /Header -->