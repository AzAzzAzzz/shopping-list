<div class="add-form">
    <form id="add-lists-form" class="form-horizontal" action="{{ route('shopping_list.store') }}" method="POST" role="form">
        <div class="form-group">
            <label for="name" class="col-sm-4 control-label">{{ __('form.list.name.label') }}</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="name"
                       placeholder="{{ __('form.list.name.placeholder') }}">
            </div>
        </div>
        <div class="form-group">
            <label for="count" class="col-sm-4 control-label">{{ __('form.list.count.label') }}</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="count" value="0"
                       placeholder="{{ __('form.list.count.placeholder') }}">
            </div>
            <div class="col-sm-2">
                <select name="unit" class="form-control">
                    @foreach($units as $unit)
                        <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="category" class="col-sm-4 control-label">{{ __('form.list.category.label') }}</label>
            <div class="col-sm-7">
                <select name="category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="comment" class="col-sm-4 control-label">{{ __('form.list.comment.label') }}</label>
            <div class="col-sm-7">
                <textarea name="comment" class="form-control" rows="2" placeholder="{{ __('form.list.comment.placeholder') }}"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-10">
                <button type="submit" class="btn btn-success add-btn">Добавить</button>
            </div>
        </div>
    </form>
</div>

<div class="col-sm-8 col-sm-offset-2">
    <table id="product-list-table" class="table table-striped table-bordered nowrap" cellspacing="0">
        <thead>
        <tr>
            <th>{{ __('views.welcome.table_name_1') }}</th>
            <th>{{ __('views.welcome.table_name_2') }}</th>
            <th>{{ __('views.welcome.table_name_3') }}</th>
            <th>{{ __('views.welcome.table_name_4') }}</th>
            <th>{{ __('views.welcome.table_name_5') }}</th>
            <th>{{ __('views.welcome.table_name_5') }}</th>
            <th>{{ __('views.welcome.table_name_5') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($shoppingLists as $list)
            <tr>
                <td data-order="{{ $list->name }}">
                    <input type="hidden" class="row-id" name="id" value="{{ $list->id }}">
                    <input type="text" class="form-control row-name" required name="table_name"
                           value="{{ $list->name }}">
                </td>
                <td data-order="{{ $list->count }}">
                    <input type="text" class="form-control table_count row-count" name="table_count"
                           value="{{ $list->count }}">
                </td>
                <td data-order="{{ $list->unit->name }}">
                    <select name="table_unit" class="form-control">
                        @foreach($units as $unit)
                            @if ($unit->id == $list->unit->id)
                                <option value="{{ $unit->id }}" selected>{{ $unit->name }}</option>
                            @else
                                <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </td>
                <td data-order="{{ $list->category->name }}">
                    <select name="table_category" class="form-control">
                        @foreach($categories as $category)
                            @if ($category->id == $list->category->id)
                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </td>
                <td data-order="{{ $list->comment }}">
                    <textarea name="table_comment" class="form-control row-comment" rows="1">{{ $list->comment }}</textarea>
                </td>
                <td>
                    {{ $list->created_at }}
                </td>
                <td>
                    <div class="table-actions">
                        <button class="btn btn-danger btn-remove">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-success btn-save">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        </button>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>{{ __('views.welcome.table_name_1') }}</th>
            <th>{{ __('views.welcome.table_name_2') }}</th>
            <th>{{ __('views.welcome.table_name_3') }}</th>
            <th>{{ __('views.welcome.table_name_4') }}</th>
            <th>{{ __('views.welcome.table_name_5') }}</th>
            <th>{{ __('views.welcome.table_name_5') }}</th>
            <th>{{ __('views.welcome.table_name_5') }}</th>
        </tr>
        </tfoot>
    </table>
</div>