<!-- Footer -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="cols-m-12">
                <p class=" text-center copyright">&copy; {{ date('Y') }} {{ config('app.name') }}. {{ __('views.backend.section.footer.copyright') }}</p>
            </div>
        </div>
    </div>
</div>
<!-- /Footer -->