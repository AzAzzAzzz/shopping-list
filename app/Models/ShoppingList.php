<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Lang;

class ShoppingList extends Model
{
    /**
     * Get the created_at.
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        Date::setLocale(Lang::locale());
        return Date::parse($value)->diffForHumans();
    }

    public function category()
    {
        return $this->hasOne(\App\Models\Category::class, 'id', 'category_id');
    }

    public function unit()
    {
        return $this->hasOne(\App\Models\Unit::class, 'id', 'unit_id');
    }
}
