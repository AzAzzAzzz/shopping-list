<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreUnit;
use App\Http\Requests\UpdateUnit;
use App\Models\Unit;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.unit.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUnit $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreUnit $request)
    {
        $unit = new Unit();
        $unit->name = $request->get('name');

        $result = $unit->save();

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('unit.add_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('unit.success');
        return response()->json($this->response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.unit.edit', ['unit' => Unit::find($id)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUnit $request
     * @param Unit $unit
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUnit $request, Unit $unit)
    {
        $unit->name = $request->get('name');

        $result = $unit->save();

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('unit.update_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('unit.update_success');
        return response()->json($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Unit::destroy('id', $id);

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('unit.destroy_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('unit.destroy_success');
        return response()->json($this->response);
    }

    /**
     * Response data for dataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => Unit::all()
        ]);
    }
}
