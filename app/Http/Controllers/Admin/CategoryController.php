<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreCategory;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests\UpdateCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategory $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCategory $request)
    {
        $category = new Category();
        $category->name = $request->get('name');

        $result = $category->save();

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('category.add_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('category.add_success');
        return response()->json($this->response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.category.edit', ['category' => Category::find($id)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategory $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCategory $request, Category $category)
    {
        $category->name = $request->get('name');

        $result = $category->save();

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('category.update_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('category.update_success');
        return response()->json($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Category::destroy('id', $id);

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('category.destroy_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('category.destroy_success');
        return response()->json($this->response);
    }

    /**
     * Response data for dataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => Category::all()
        ]);
    }
}
