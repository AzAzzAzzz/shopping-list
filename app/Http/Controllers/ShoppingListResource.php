<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreList;
use App\Http\Requests\UpdateList;
use App\Models\Category;
use App\Models\Unit;
use Illuminate\Support\Facades\Auth;
use App\Models\ShoppingList;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;
use App\Models\Auth\User\User;

class ShoppingListResource extends Controller
{
    /**
     * WelcomeResource constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreList $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreList $request)
    {
        // Checking the data in the table
        if (
            ! Unit::whereId($request->get('unit'))->exists() ||
            ! Category::whereId($request->get('category'))->exists() ||
            ! User::whereId(Auth::user()->id)->exists()
        ) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('shopping_list.add_list_error');
            return response()->json($this->response);
        }

        $shoppingList = new ShoppingList();
        $shoppingList->name = $request->get('name');
        $shoppingList->count = $request->get('count');
        $shoppingList->user_id = Auth::user()->id;
        $shoppingList->unit_id = $request->get('unit');
        $shoppingList->category_id = $request->get('category');
        $shoppingList->comment = $request->get('comment');

        $result = $shoppingList->save();

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('shopping_list.add_list_error');
            return response()->json($this->response);
        }

        // Destroy item from the cache
        Cache::forget('shoppingList.' . Auth::user()->id);

        $this->response['message'] = Lang::get('shopping_list.add_list_success');
        $this->response['data'] = $shoppingList;
        return response()->json($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateList  $request
     * @param  ShoppingList  $shoppingList
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateList $request, ShoppingList $shoppingList)
    {
        $shoppingList->name = $request->get('table_name');
        $shoppingList->count = $request->get('table_count');
        $shoppingList->unit_id = $request->get('table_unit');
        $shoppingList->category_id = $request->get('table_category');
        $shoppingList->comment = $request->get('table_comment');

        $result = $shoppingList->save();

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('shopping_list.update_list_error');
            return response()->json($this->response);
        }

        // Destroy item from the cache
        Cache::forget('shoppingList.' . Auth::user()->id);

        $this->response['message'] = Lang::get('shopping_list.update_list_success');
        return response()->json($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = ShoppingList::destroy('id', $id);

        if ( ! $result) {
            $this->response['error'] = true;
            $this->response['message'] = Lang::get('shopping_list.destroy_list_error');
            return response()->json($this->response);
        }

        // Destroy item from the cache
        Cache::forget('shoppingList.' . Auth::user()->id);

        $this->response['message'] = Lang::get('shopping_list.destroy_list_success');
        return response()->json($this->response);
    }

    /**
     * Response data for dataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => Category::all()
        ]);
    }
}
