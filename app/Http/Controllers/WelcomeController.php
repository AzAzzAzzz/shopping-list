<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Unit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\ShoppingList;

class WelcomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieving data from the cache
        $categories = Cache::remember('categories', 30, function () {
            return Category::all();
        });

        $units = Cache::remember('units', 30, function () {
            return Unit::all();
        });

        if (Auth::check()) {
            $shoppingLists = Cache::remember('shoppingList.' . Auth::user()->id, 10, function () {
                return ShoppingList::where('user_id', Auth::user()->id)
                    ->with(['unit', 'category'])
                    ->get();
            });
        } else {
            $shoppingLists = [];
        }

        return view('welcome', [
            'categories' => $categories,
            'units' => $units,
            'shoppingLists' => $shoppingLists
        ]);
    }
}
