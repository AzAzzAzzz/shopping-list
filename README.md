## Installation
1. Install Composer using detailed installation instructions [here](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
2. Install Node.js using detailed installation instructions [here](https://nodejs.org/en/download/package-manager/)

3. Clone repository
```
$ git clone  https://gitlab.com/AzAzzAzzz/shopping-list.git
```

4. Change into the working directory
```
$ cd shopping-list
```

5. Copy `.env.example` to `.env` and modify according to your environment
```
$ cp .env.example .env
```

6. Install composer dependencies
```
$ composer install --prefer-dist
```

7. An application key can be generated with the command
```
$ php artisan key:generate
```

8. Execute following commands to install other dependencies
```
$ npm install
$ npm run dev
```

8. Run these commands to create the tables within the defined database and populate seed data
```
$ php artisan migrate --seed
```

9. Сonfigure the virtual host on the shopping-list/public
